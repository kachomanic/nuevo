<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Familias */

$this->title = 'Update Familias: ' . ' ' . $model->codFamilia;
$this->params['breadcrumbs'][] = ['label' => 'Familias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codFamilia, 'url' => ['view', 'id' => $model->codFamilia]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="familias-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
