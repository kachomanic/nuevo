<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Diocesis;

/* @var $this yii\web\View */
/* @var $model backend\models\Parroquias */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="parroquias-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'nombre')->textInput(['maxlength' => 80]) ?>
    <?= $form->field($model, 'codDiocesis')->dropDownList(
	ArrayHelper::map(Diocesis::find()->all(),'codDiocesis','nombre'),
	['prompt'=>'Selecciona diocesis']
    ) ?>


    <?= $form->field($model, 'municipio')->textInput(['maxlength' => 60]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
