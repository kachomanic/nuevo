<style>
body{
   background-color: #ccf;
}

</style>
<?php
/* @var $this yii\web\View */

$this->title = 'Aplicación para el control de notas';
?>
<div class="site-index">

    <div class="jumbotron">
        <h2>Sistema de Gestión de notas</h2>

        <p class="lead">Haga su selección para comenzar.</p>

 <p><a class="btn btn-lg btn-success" href="index.php?r=docentes">Docentes</a>
    <a class="btn btn-lg btn-success" href="index.php?r=notas">Notas</a>
    <a class="btn btn-lg btn-success" href="index.php?r=parroquias">Parroquias</a>   
    <a class="btn btn-lg btn-success" href="index.php?r=asignaturas">Asignaturas</a>
    <a class="btn btn-lg btn-success" href="index.php?r=diocesis">Diocesis</a>
    <a class="btn btn-lg btn-success" href="index.php?r=especialidades">Especialidades</a>
</p>
<p>
    <a class="btn btn-lg btn-success" href="index.php?r=estudios">Estudios</a>
    <a class="btn btn-lg btn-success" href="index.php?r=facultades">Facultades</a>
    <a class="btn btn-lg btn-success" href="index.php?r=familias">Familias</a>
    <a class="btn btn-lg btn-success" href="index.php?r=grupos">Grupos</a>
    <a class="btn btn-lg btn-success" href="index.php?r=hermanos">Hermanos</a>
    <a class="btn btn-lg btn-success" href="#">Matriculas</a>
</p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Reportes</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Configuración</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Ayuda del sistema</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
