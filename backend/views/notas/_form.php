<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Notas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="notas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codGrupo')->textInput() ?>

    <?= $form->field($model, 'codMatricula')->textInput() ?>

    <?= $form->field($model, 'codEstudiante')->textInput() ?>

    <?= $form->field($model, 'presencial_especial')->checkbox() ?>

    <?= $form->field($model, 'tutoria')->checkbox() ?>

    <?= $form->field($model, 'nota')->textInput(['maxlength' => 3]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
