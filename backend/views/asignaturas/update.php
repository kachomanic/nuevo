<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Asignaturas */

$this->title = 'Update Asignaturas: ' . ' ' . $model->codAsignatura;
$this->params['breadcrumbs'][] = ['label' => 'Asignaturas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codAsignatura, 'url' => ['view', 'id' => $model->codAsignatura]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="asignaturas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
