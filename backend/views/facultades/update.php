<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Facultades */

$this->title = 'Update Facultades: ' . ' ' . $model->codFacultad;
$this->params['breadcrumbs'][] = ['label' => 'Facultades', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codFacultad, 'url' => ['view', 'id' => $model->codFacultad]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="facultades-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
