<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Facultades */

$this->title = 'Create Facultades';
$this->params['breadcrumbs'][] = ['label' => 'Facultades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="facultades-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
