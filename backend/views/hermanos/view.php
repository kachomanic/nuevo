<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Hermanos */

$this->title = $model->codHermano;
$this->params['breadcrumbs'][] = ['label' => 'Hermanos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hermanos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->codHermano], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->codHermano], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codHermano',
            'codEstudiante',
            'cantHermanos',
            'cantHermanas',
        ],
    ]) ?>

</div>
