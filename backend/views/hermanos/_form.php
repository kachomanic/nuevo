<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Hermanos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hermanos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codEstudiante')->textInput() ?>

    <?= $form->field($model, 'cantHermanos')->textInput(['maxlength' => 2]) ?>

    <?= $form->field($model, 'cantHermanas')->textInput(['maxlength' => 2]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
