<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Hermanos */

$this->title = 'Create Hermanos';
$this->params['breadcrumbs'][] = ['label' => 'Hermanos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hermanos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
