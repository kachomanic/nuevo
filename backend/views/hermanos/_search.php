<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\HermanosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hermanos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codHermano') ?>

    <?= $form->field($model, 'codEstudiante') ?>

    <?= $form->field($model, 'cantHermanos') ?>

    <?= $form->field($model, 'cantHermanas') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
