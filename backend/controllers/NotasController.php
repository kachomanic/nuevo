<?php

namespace backend\controllers;

use Yii;
use backend\models\Notas;
use backend\models\NotasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NotasController implements the CRUD actions for Notas model.
 */
class NotasController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Notas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NotasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Notas model.
     * @param integer $codGrupo
     * @param integer $codMatricula
     * @param integer $codEstudiante
     * @return mixed
     */
    public function actionView($codGrupo, $codMatricula, $codEstudiante)
    {
        return $this->render('view', [
            'model' => $this->findModel($codGrupo, $codMatricula, $codEstudiante),
        ]);
    }

    /**
     * Creates a new Notas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Notas();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codGrupo' => $model->codGrupo, 'codMatricula' => $model->codMatricula, 'codEstudiante' => $model->codEstudiante]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Notas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $codGrupo
     * @param integer $codMatricula
     * @param integer $codEstudiante
     * @return mixed
     */
    public function actionUpdate($codGrupo, $codMatricula, $codEstudiante)
    {
        $model = $this->findModel($codGrupo, $codMatricula, $codEstudiante);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codGrupo' => $model->codGrupo, 'codMatricula' => $model->codMatricula, 'codEstudiante' => $model->codEstudiante]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Notas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $codGrupo
     * @param integer $codMatricula
     * @param integer $codEstudiante
     * @return mixed
     */
    public function actionDelete($codGrupo, $codMatricula, $codEstudiante)
    {
        $this->findModel($codGrupo, $codMatricula, $codEstudiante)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Notas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $codGrupo
     * @param integer $codMatricula
     * @param integer $codEstudiante
     * @return Notas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codGrupo, $codMatricula, $codEstudiante)
    {
        if (($model = Notas::findOne(['codGrupo' => $codGrupo, 'codMatricula' => $codMatricula, 'codEstudiante' => $codEstudiante])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
