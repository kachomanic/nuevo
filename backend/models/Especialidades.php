<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "especialidades".
 *
 * @property integer $codEspecialidad
 * @property string $nombre
 *
 * @property Docentes[] $docentes
 */
class Especialidades extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'especialidades';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 80]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'codEspecialidad' => 'Cod Especialidad',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocentes()
    {
        return $this->hasMany(Docentes::className(), ['especialidad' => 'codEspecialidad']);
    }
}
