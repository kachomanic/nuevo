<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "diocesis".
 *
 * @property integer $codDiocesis
 * @property string $nombre
 *
 * @property Parroquias[] $parroquias
 */
class Diocesis extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'diocesis';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 70]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'codDiocesis' => 'Cod Diocesis',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParroquias()
    {
        return $this->hasMany(Parroquias::className(), ['codDiocesis' => 'codDiocesis']);
    }
}
