<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "asignaturas".
 *
 * @property integer $codAsignatura
 * @property string $numAsignatura
 * @property string $nombre
 * @property string $especificaciones
 * @property string $totalHoras
 * @property string $credito
 *
 * @property Grupos[] $grupos
 * @property PlanEstudio[] $planEstudios
 * @property Facultades[] $codFacultads
 * @property Prerrequisitos[] $prerrequisitos
 */
class Asignaturas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'asignaturas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['numAsignatura', 'nombre', 'totalHoras', 'credito'], 'required'],
            [['totalHoras', 'credito'], 'number'],
            [['numAsignatura'], 'string', 'max' => 10],
            [['nombre'], 'string', 'max' => 70],
            [['especificaciones'], 'string', 'max' => 60],
            [['numAsignatura'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'codAsignatura' => 'Cod Asignatura',
            'numAsignatura' => 'Num Asignatura',
            'nombre' => 'Nombre',
            'especificaciones' => 'Especificaciones',
            'totalHoras' => 'Total Horas',
            'credito' => 'Credito',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrupos()
    {
        return $this->hasMany(Grupos::className(), ['codAsignatura' => 'codAsignatura']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanEstudios()
    {
        return $this->hasMany(PlanEstudio::className(), ['codAsignatura' => 'codAsignatura']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodFacultads()
    {
        return $this->hasMany(Facultades::className(), ['codFacultad' => 'codFacultad'])->viaTable('plan_estudio', ['codAsignatura' => 'codAsignatura']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrerrequisitos()
    {
        return $this->hasMany(Prerrequisitos::className(), ['codPrerrequisito' => 'codAsignatura']);
    }
}
