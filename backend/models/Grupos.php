<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "grupos".
 *
 * @property integer $codGrupo
 * @property string $numGrupo
 * @property integer $codAsignatura
 * @property integer $codDocente
 *
 * @property Asignaturas $codAsignatura0
 * @property Docentes $codDocente0
 * @property Notas[] $notas
 */
class Grupos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'grupos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codAsignatura', 'codDocente'], 'required'],
            [['codAsignatura', 'codDocente'], 'integer'],
            [['numGrupo'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'codGrupo' => 'Cod Grupo',
            'numGrupo' => 'Num Grupo',
            'codAsignatura' => 'Cod Asignatura',
            'codDocente' => 'Cod Docente',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodAsignatura0()
    {
        return $this->hasOne(Asignaturas::className(), ['codAsignatura' => 'codAsignatura']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodDocente0()
    {
        return $this->hasOne(Docentes::className(), ['codDocente' => 'codDocente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotas()
    {
        return $this->hasMany(Notas::className(), ['codGrupo' => 'codGrupo']);
    }
}
