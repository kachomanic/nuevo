<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "estudiantes".
 *
 * @property integer $codEstudiante
 * @property string $carnetEst
 * @property string $fechaIngreso
 * @property integer $codParroquia
 * @property string $teleDomicilio
 * @property string $direccionDomicilio
 * @property string $cedula
 * @property string $fechaNac
 * @property string $lugarNac
 * @property string $sApellido
 * @property string $pApellido
 * @property string $sNombre
 * @property string $pNombre
 *
 * @property Parroquias $codParroquia0
 * @property EstudiosEstudiantes[] $estudiosEstudiantes
 * @property FamiliasEstudiantes[] $familiasEstudiantes
 * @property Hermanos[] $hermanos
 * @property Notas[] $notas
 */
class Estudiantes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'estudiantes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fechaIngreso', 'codParroquia', 'teleDomicilio', 'direccionDomicilio', 'cedula', 'fechaNac', 'lugarNac', 'pApellido', 'pNombre'], 'required'],
            [['fechaIngreso', 'fechaNac'], 'safe'],
            [['codParroquia'], 'integer'],
            [['carnetEst'], 'string', 'max' => 15],
            [['teleDomicilio', 'cedula'], 'string', 'max' => 20],
            [['direccionDomicilio'], 'string', 'max' => 100],
            [['lugarNac'], 'string', 'max' => 70],
            [['sApellido', 'pApellido', 'sNombre', 'pNombre'], 'string', 'max' => 50],
            [['cedula'], 'unique'],
            [['carnetEst'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'codEstudiante' => 'Cod Estudiante',
            'carnetEst' => 'Carnet Est',
            'fechaIngreso' => 'Fecha Ingreso',
            'codParroquia' => 'Cod Parroquia',
            'teleDomicilio' => 'Tele Domicilio',
            'direccionDomicilio' => 'Direccion Domicilio',
            'cedula' => 'Cedula',
            'fechaNac' => 'Fecha Nac',
            'lugarNac' => 'Lugar Nac',
            'sApellido' => 'S Apellido',
            'pApellido' => 'P Apellido',
            'sNombre' => 'S Nombre',
            'pNombre' => 'P Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodParroquia0()
    {
        return $this->hasOne(Parroquias::className(), ['codParroquia' => 'codParroquia']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstudiosEstudiantes()
    {
        return $this->hasMany(EstudiosEstudiantes::className(), ['codEstudiante' => 'codEstudiante']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFamiliasEstudiantes()
    {
        return $this->hasMany(FamiliasEstudiantes::className(), ['codEstudiante' => 'codEstudiante']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHermanos()
    {
        return $this->hasMany(Hermanos::className(), ['codEstudiante' => 'codEstudiante']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotas()
    {
        return $this->hasMany(Notas::className(), ['codEstudiante' => 'codEstudiante']);
    }
}
