<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "docentes".
 *
 * @property integer $codDocente
 * @property string $carnetDocente
 * @property string $nombres
 * @property string $apellidos
 * @property string $cedula
 * @property string $telefono
 * @property string $correo
 * @property integer $especialidad
 *
 * @property Especialidades $especialidad0
 * @property Grupos[] $grupos
 */
class Docentes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'docentes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombres', 'apellidos', 'cedula', 'telefono', 'especialidad'], 'required'],
            [['especialidad'], 'integer'],
            [['carnetDocente'], 'string', 'max' => 15],
            [['nombres', 'apellidos'], 'string', 'max' => 70],
            [['cedula'], 'string', 'max' => 20],
            [['telefono', 'correo'], 'string', 'max' => 40],
            [['cedula'], 'unique'],
            [['carnetDocente'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'codDocente' => 'Cod Docente',
            'carnetDocente' => 'Carnet Docente',
            'nombres' => 'Nombres',
            'apellidos' => 'Apellidos',
            'cedula' => 'Cedula',
            'telefono' => 'Telefono',
            'correo' => 'Correo',
            'especialidad' => 'Especialidad',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEspecialidad0()
    {
        return $this->hasOne(Especialidades::className(), ['codEspecialidad' => 'especialidad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrupos()
    {
        return $this->hasMany(Grupos::className(), ['codDocente' => 'codDocente']);
    }
}
